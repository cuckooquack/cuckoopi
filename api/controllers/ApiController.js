/**
 * ApiController
 *
 * @module		:: Controller
 * @description	:: Contains logic for handling requests.
 */

// Instantiate array of LEDs
var tab_led = new Array();
tab_led[1] = new Array(2,3);      
tab_led[2] = new Array(4,5);
tab_led[3] = new Array(6,7);
tab_led[4] = new Array(8,9);


// Initialize boards and hardware
var j5 = require("johnny-five"),
    board, lcd;

var ports = [
    { id: "board", port: "/dev/ttyUSB0" },
    { id: "board_screen", port: "/dev/ttyACM0" }
 ];

boards = new j5.Boards(ports).on("ready", function() {
    // Boards are initialized!
    // Set pinmod outup for LEDs (2 to 9)
  	for (var pin=2;pin<10;pin++)
	{
		boards[0].pinMode( pin, 1 );
	}
	// Set pinmod output for Cuuuuuckooooooo
	boards[0].pinMode( 10, 1 ); // cuckoo outside
	boards[0].pinMode( 11, 1 ); // cuckoo back home
	  
	// Create LCD object
	lcd = new j5.LCD({
	    // LCD pin name  RS  EN  DB4 DB5 DB6 DB7
	    // Arduino pin # 7    8   9   10  11  12
	    pins: [ 8, 9, 4, 5, 6, 7 ],
	    rows: 2,
	    cols: 16,
	    board: board_screen
  	});
  	
  	// LCD backlight control
  	boards[1].pinMode( 10, 1 ); // cuckoo outside
  	
});



module.exports = {

  /* 
	EXAMPLE JSON
	{
	    "cuckoo": 3000,
	    "message": "This is a message to display on the screen",
	    "sound": "quack.mp3",
	    "leds": [
	        {
	           "id": "9",
	           "effect": "strobe",
	           "timeout": 3000
	        }
	    ]
	}
  */
  
  command: function (req, res) {
    
	/*
	 if(!req.isJson) {
		// We expect JSON!
		res.send('Sorry, We need JSON', 404);
	}
	*/
	
	// get the commande from received JSON
	var cuckoo = req.param('cuckoo');
	var message = req.param('message');
	var sound = req.param('sound');
	var leds = req.param('leds');
	
	// CUCKOO ACTIONS
	if(cuckoo) {
		// Set him free (just for a few sec...)
		boards[0].digitalWrite(11, 0)
		boards[0].digitalWrite(10, 1);
	  	setTimeout(function(){
	  		boards[0].digitalWrite(11, 1);
	  		boards[0].digitalWrite(10, 0);
	  		}, cuckoo); // timeout is the cuckoo value
	  	boards[0].digitalWrite(11, 0)
	}
	
	// LEDS ACTIONS
	if(leds) {
		var num_leds = leds.length;
	    element = null;
		for (var i = 0; i < num_leds; i++) {
		  // for each LED we receive
		  var led = leds[i];
		  var led_num = led['id'];
		  var led_effect = led['effect'];
		  var led_timeout = led['timeout'];
		  if(led_num) {
		  	// Create 2 LED objects
		  	//led1 = new j5.Led({pin: 8, board: board});
		  	led1 = new j5.Led({pin: tab_led[led_num][0], board: board});
		  	led2 = new j5.Led({pin: tab_led[led_num][1], board: board});
		  	
		  	// Switch on type of effect
		  	if(led_effect) {
		  		switch(led_effect)
				{
				case "on":
				  led1.on(); led2.on();
		  		  setTimeout(function(){led1.stop().off(); led2.stop().off()}, led_timeout);
				  break;
				case "strobe":
				  led1.strobe(50); led2.strobe(50);
		  		  setTimeout(function(){led1.stop().off(); led2.stop().off()}, led_timeout);
				  break;
				case "blink":
				  led1.strobe(200); led2.strobe(200);
		  		  setTimeout(function(){led1.stop().off(); led2.stop().off()}, led_timeout);
				  break;
				default:
				  
				}
		  	} // end if effect
		  	
		  } // end if led number
		} // End for leds array
	}
	
	// MESSAGE ACTION
	if (message) {
		
		message_length = message.length;
		lcd.clear();
		lcd.home();
		//boards[1].digitalWrite( 10, 1 );
		
		// set the display to automatically scroll:
		//lcd.autoscroll();
		lcd.noAutoscroll();
		
		lcd.print(message);
		
		//for (var i = 0; i < message_length ; i++){
		//    lcd.print(message.charAt(i));
			//setTimeout(function(){}, 1000);
		//}
		// turn off automatic scrolling
		//lcd.noAutoscroll();
		// clear screen for the next loop:
		setTimeout(function(){lcd.clear().noDisplay()/*; boards[1].digitalWrite( 10, 0 );*/}, 5000);
	}
	
	      
    res.send("This is your message: "+message, 200);
  }
  

};
