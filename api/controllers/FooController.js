/**
 * FooController
 *
 * @module		:: Controller
 * @description	:: Contains logic for handling requests.
 */

// Initialize boards and hardware
/*
var j5 = require("johnny-five"),
    board, led;

board = new j5.Board();

board.on("ready", function() {

  // Create a standard `led` hardware instance
  led = new j5.Led({
    // Use PWM pin 9 for fading example
    pin: 9
  });
});
*/

// Create a standard `led` hardware instance
/*
  led = new j5.Led({
    // Use PWM pin 9 for fading example
    pin: 9
  });
*/

module.exports = {

  /* e.g.
  sayHello: function (req, res) {
    res.send('hello world!');
  }
  */

  
  ledOn: function (req, res) {
    
    //var j5 = require("johnny-five");
	//var board = new j5.Board();
	
	var LEDPIN = 8;
	var LEDPIN2 = 9;
	var OUTPUT = 1;
	
	//board.on("ready", function(){
	
	  // Set pin 13 to OUTPUT mode
	  board.pinMode(LEDPIN, OUTPUT);
	  board.pinMode(LEDPIN2, OUTPUT);
	  // Light it up!
	  board.digitalWrite(LEDPIN, 1);
	  board.digitalWrite(LEDPIN2, 0);
	//});
    
    res.send('LED is ON');
  },	
 
  ledOff: function (req, res) {
    
    //var j5 = require("johnny-five");
	//var board = new j5.Board();
	
	var LEDPIN = 8;
	var LEDPIN2 = 9;
	var OUTPUT = 1;
	
	  // Set pins 8 and 9 to OUTPUT mode
	  board.pinMode(LEDPIN, OUTPUT);
	  board.pinMode(LEDPIN2, OUTPUT);
	
	  // Light it off!
	  board.digitalWrite(LEDPIN, 0);
	  board.digitalWrite(LEDPIN2, 1);

    
    res.send('LED is OFF');
  },
  
  
  ledPulse: function (req, res) {
    
	  
	  led.pulse();
	  // Turn off the led pulse loop after 10 seconds (shown in ms)
	  board.wait( 10000, function() {
	
	    led.stop().off();
	
	  });
    
    res.send('LED has pulsed');
  }
  
 
 	
  

};
